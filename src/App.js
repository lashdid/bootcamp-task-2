import { CustomAppBar } from "./components/CustomAppBar";
import { Content } from "./components/Content";
import { CustomModal } from "./components/CustomModal";
import { useState } from "react";
import { ListContent } from "./components/ListContent";

export default function App() {
  const [isModalOpen, setModalOpen] = useState(false);
  const [data, setData] = useState([]);
  return (
    <main>
      <CustomAppBar onButtonClick={() => setModalOpen(true)} />
      {data.length === 0 ? <Content /> : <ListContent listArray={data} />}
      <CustomModal
        isModalOpen={isModalOpen}
        onModalClose={() => setModalOpen(false)}
        onFormSave={(item) => {
          item && setData([...data, item]);
          setModalOpen(false);
        }}
      />
    </main>
  );
}
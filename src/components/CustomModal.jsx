import { Button, TextField } from "@mui/material";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import { useState } from "react";

export const CustomModal = ({isModalOpen, onModalClose, onFormSave}) => {
  const [name, setName] = useState("")
  const [address, setAddress] = useState("")
  const [hobby, setHobby] = useState("")
  return (
    <Modal
      open={isModalOpen}
      onClose={onModalClose}
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <Box style={{
        minWidth: '20rem',
        display: 'flex',
        flexDirection: 'column',
        gap: '1rem',
        backgroundColor: 'white',
        padding: '2rem',
        borderRadius: '1rem'
      }}>
        <Typography variant="h4" align="center">
          Add User
        </Typography>
        <TextField value={name} onChange={(e) => setName(e.target.value)} fullWidth label="Name" variant="outlined" />
        <TextField value={address} onChange={(e) => setAddress(e.target.value)} fullWidth label="Address" variant="outlined" />
        <TextField value={hobby} onChange={(e) => setHobby(e.target.value)} fullWidth label="Hobby" variant="outlined" />
        <Button onClick={() => {
          let checker = name && address && hobby
          onFormSave(checker ? {name, address, hobby} : null)
          setName("")
          setAddress("")
          setHobby("")
        }} variant='contained' style={{alignSelf: 'center', paddingInline: '3rem', color: 'black', backgroundColor: '#00c2cb', fontSize: '18px', textTransform: 'none', borderRadius: '50px'}}>Save</Button>
      </Box>
    </Modal>
  );
};

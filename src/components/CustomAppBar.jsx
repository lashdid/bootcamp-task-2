import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

export const CustomAppBar = ({onButtonClick}) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" style={{backgroundColor: '#004aad'}}>
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            My App
          </Typography>
          <Button onClick={onButtonClick} variant='contained' style={{backgroundColor: '#00c2cb', fontSize: '18px', textTransform: 'none', borderRadius: '50px'}}>Add User</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
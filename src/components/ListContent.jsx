import { Box, Card, Typography } from '@mui/material'
import React from 'react'

export const ListContent = ({listArray}) => {
  return (
    <Box style={{display: 'flex', flexDirection: 'column', gap: '1rem'}}>
      {listArray.map((item, idx) => (
        <Card key={idx} style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '1rem'}}>
          <div>
            <Typography variant="h4" color="initial">{item.name}</Typography>
            <Typography variant="h6" color="initial">{item.address}</Typography>
          </div>
          <Typography variant="h4" color="initial">{item.hobby}</Typography>
        </Card>
      ))}
    </Box>
  )
}
